import React, { Component } from 'react';
import './Forecast.css';
import OwlCarousel from 'react-owl-carousel2';
import Icon from '../../components/WeatherIcons/Icons';
import {connect} from 'react-redux';
import {getForecast} from '../../actions/WeatherAction';

class Forecast extends Component {

  componentWillMount() {
      this.props.getForecast();
  }

  render() {
    const options = {
        nav: false,
        lazyLoad:true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 7
            }
        }
    };

    let forecastList = '';
    let locTime, day, dayName;

    if (this.props.for !== undefined) {
        locTime = this.props.locationTime;
        forecastList = (
            this.props.for.forecastday.map((d, index) => {
                day = new Date(parseInt(d.date_epoch, 10) * 1000).getDay();

                return <div key={index}>
                         <Icon code={d.day.condition.code} time='2018-08-02 13:00' />
                         <p className='day'>
                             {day === 0 ? 'Sunday' : null}
                             {day === 1 ? 'Monday' : null}
                             {day === 2 ? 'Tuesday' : null}
                             {day === 3 ? 'Wednesday' : null}
                             {day === 4 ? 'Thursday' : null}
                             {day === 5 ? 'Friday' : null}
                             {day === 6 ? 'Saturday' : null}
                         </p>
                         <h4>
                             <span className='high'>{d.day.maxtemp_c.toFixed(0)}° </span> 
                             <span className='separator'>/</span>
                             <span className='min'> &nbsp; {d.day.mintemp_c.toFixed(0)}°</span>
                         </h4>
                         <p className='condition'>{d.day.condition.text}</p>
                     </div>
            })
        )
    } else {
        forecastList = (
            <div></div>
        )
    }

    return (
      <div className='forecastParent'>
            <OwlCarousel options={options}> 
                {forecastList}
            </OwlCarousel>
      </div>
    )
  }
}

const mapStateToProps = state => ({
    for: state.forecastWeather.forecast.forecast,
    locationTime: state.forecastWeather.locationTime,
}) 

//  {
//     forecast: state.forecastWeather.forecast.forecast
// }
export default connect(mapStateToProps, {getForecast})(Forecast);
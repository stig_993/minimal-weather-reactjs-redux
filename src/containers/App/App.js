import React, { Component } from 'react';
import './App.css';
import Weather from '../Weather/Weather';
import {Route, Switch} from 'react-router-dom';
import {Provider} from 'react-redux';
import store from '../../store';


class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Switch>
          <Route component={Weather} path="/" exact />
        </Switch>
      </Provider>
    );
  }
}

export default App;

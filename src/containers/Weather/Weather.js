import React, { Component } from 'react';
import {connect} from 'react-redux';
import './Weather.css';
import {getWeather, getCities, getMyLocWeather, getForecast} from '../../actions/WeatherAction';
import Footer from '../../components/footer/Footer';
import Forecast from '../Forecast/Forecast';

import Icons from '../../components/WeatherIcons/Icons';
import SearchForm from '../../components/searchForm/Search';
import CitiesList from '../../components/citiesList/citiesList';
import locationIcon from '../../assets/img/location-pin.png';
import warningIcon from '../../assets/img/warning.png';

class Weather extends Component {

  state = {
    city: null
  }

  componentWillMount() {
    this.props.getWeather();
    this.props.getForecast();
  }

  componentDidMount() {
    this.getLocation(document.getElementById('city').click);
  }

  getLocation = () => {
    navigator.geolocation.getCurrentPosition(location => {
      this.setState({
        location: {
          lat: location.coords.latitude,
          lon: location.coords.longitude
        }
      })
    });
  }

  componentWillReceiveProps(props) {
    if (props.cities.length !== 0) {
      this.props.cities.push(props.cities);
    }
  }

  searchCity = (e) => {
    this.setState({[e.target.name]: e.target.value})
    this.props.getCities(e.target.value)
  }

  getWeather = (city) => {
    this.props.getWeather(city);
    this.props.getForecast(city);
  }

  findMe = () => {
    if (this.state.location !== undefined) {
      this.props.getMyLocWeather(this.state.location);
    } 
  }

  render() {

    // Automatic refresh 
    setInterval(function() {
      window.location.reload();
    }, 1800000)

    let content = '';
    let location = '';
    let current = '';
    let locTime = '';

    if (Object.keys(this.props.weather_now).length !== 0) {
      location = this.props.weather_now.location;
      current = this.props.weather_now.current;
      locTime = this.props.locationTime;

      content = (
        <div className='container'>
          <h3>{location.name}, {location.country}</h3>
          <Icons time={locTime} code={current.condition.code} /> 
          <h2>{current.temp_c.toFixed(0)}°</h2>
          <p>{current.condition.text}</p>

          <h4 onClick={this.getLocation}>Wind: <span>{current.wind_kph.toFixed(0)} KM/h</span></h4>
          <h4>Wind Direction: <span>{current.wind_kph === 0 ? '-' : current.wind_dir }</span></h4>
          <h4>Feels like: <span>{current.feelslike_c.toFixed(0)}°</span></h4>
        </div>
      )
    }

    let citiesList = '';
    if (this.props.cities.lenght !== 0) {
      citiesList = (
        <div className='citiesList'>
          <ul>
            {this.props.cities.map(city => {
              return <li key={city.id} onClick={() => this.getWeather(city.name)}>
                  {city.name}
              </li>
            })}
          </ul>
        </div>
      )
    }


    let locationPin = '';
    if (this.state.location !== undefined) {
      locationPin = (
        <div className='findLocation' onClick={this.findMe}>
          <img src={locationIcon} alt=""/>
          <h3 className='yourLocation'>
            Your location is: {this.state.location.lat.toFixed(5)}, {this.state.location.lon.toFixed(5)}
            <br />
            <small>(Get weather for your current location)</small> 
          </h3>
        </div>
      )
    } else {
      locationPin = (
        <div className='findLocation noGps' onClick={this.findMe}>
          <img src={warningIcon}/> 
          <h3>No GPS signal <br /> <small>(You can't check weather for your current location)</small> </h3>
        </div>
      )
    }

    return (
      <div className='container'>
        <p>Last update: {this.props.time}h</p>
        <SearchForm search={this.searchCity} clearInput={this.clearInput} />
        <CitiesList list={this.props.cities} getWeather={this.getWeather} />
        {content}
        <Forecast />
        {locationPin}
        <Footer />
        <p>Weather Source | <a href='https://apixu.com' target='_blank'>apixu.com</a> </p>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  weather_now: state.currentWeather.weather_now,
  time: state.currentWeather.time,
  locationTime: state.currentWeather.locationTime,
  cities: state.currentWeather.cities,
  forecast: state.forecastWeather.forecast
})

export default connect(mapStateToProps, {getWeather, getCities, getMyLocWeather, getForecast})(Weather);
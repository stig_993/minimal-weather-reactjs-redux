import React from 'react';
import './citiesList.css';

const CitiesList = (props) => {
    let citiesList = '';
    if (props.list.length !== 0) {
        citiesList = (
          <ul>
            {props.list.map(city => {
              return <li key={city.id} onClick={() => props.getWeather(city.name)}>
                  {city.name}
              </li>
            })}
          </ul>
        )
    }
    return (
        <div className='citiesList'>
            {citiesList}
        </div>
    )
}

export default CitiesList;
import React from 'react';
import './Footer.css';
import heart from  '../../assets/img/like.png';

const Footer = () => {
    return (
        <footer>
            <p>Made with <img src={heart} alt=""/> by Zarko Mladenovic</p>
        </footer>
    )
}

export default Footer;
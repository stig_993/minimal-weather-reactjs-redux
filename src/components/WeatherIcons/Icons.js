import React from 'react';
import './Icons.css';
import mist from '../../assets/img/haze.png';
import moon from '../../assets/img/moon.png';

const Icons = (props) => {

    let condition = '';

    let currentTime;
    let sunrise = new Date();
    let sunset = new Date();

    sunrise.setHours(6,0,0,0);
    sunset.setHours(20,0,0,0);

    if (props.time.substring(11)[1] == ':') {
        currentTime = props.time.substring(11)[0];
    } else {
        currentTime = props.time.substring(11)[0] + props.time.substring(11)[1];
    }
    

    if (props.code === 1273 || props.code === 1276) {
        condition = (
            <div className="icon thunder-storm">
                <div className="cloud"></div>
                <div className="lightning">
                <div className="bolt"></div>
                <div className="bolt"></div>
                </div>
            </div>
        )
    } else if (props.code === 1003) {
        // Partly Cloudy
        if (currentTime > sunrise.getHours() && currentTime < sunset.getHours()) {
            condition = (
                <div className="icon cloudy">
                    <div className="icon sunny">
                        <div className="sun">
                            <div className="rays"></div>
                        </div>
                    </div>
                    <div className="cloud"></div>
                    <div className="cloud"></div>
                </div>
            )
        } else {
            condition = (
              <div className="icon cloudy">
                  <div className="icon cloudy">
                    <img src={moon} className='moon' alt=""/>
                  </div>
                  <div className="cloud"></div>
                  <div className="cloud"></div>
              </div>
            )
        }
    }

    else if (props.code === 1006 || props.code === 1009) {
        // Cloudy
        condition = (
            <div className="icon cloudy">
                <div className="cloud"></div>
                <div className="cloud"></div>
            </div>
        )
    }

    else if (props.code === 1189 || props.code === 1186) {
        if (currentTime > sunrise.getHours() && currentTime < sunset.getHours()) {
            condition = (
                <div className="icon sun-shower">
                    <div className="cloud"></div>
                        <div className="sun">
                            <div className="rays"></div>
                        </div>
                    <div className="rain"></div>
                </div>
            )
        } else {
            condition = (
                <div className="icon sun-shower">
                    <div className="cloud"></div>
                        <div className="icon cloudy">
                            <img src={moon} className='moon' alt=""/>
                        </div>
                    <div className="rain"></div>
                </div>
            )
        }
    }

    else if (props.code === 1243 || props.code === 1063 || props.code === 1189 || props.code === 1240 || props.code === 1183) {
        condition = (
           <div className="icon rainy">
                <div className="cloud"></div>
                <div className="rain"></div>
           </div>
        )
    }

    else if (props.code === 1030) {
        // Mist
        condition = (
          <div className="icon cloudy">
            <img src={mist}  className='mist' alt=""/>
          </div>
        )
    }

    else if (props.code === 1213 || props.code === 1066 || props.code === 1114 || props.code === 1210 || 
        props.code === 1216 || props.code === 1219 || props.code === 1222 || props.code === 1225 || 
        props.code === 1255 || props.code === 1258 ) {
        // Snow
        condition = (
          <div class="icon flurries">
            <div class="cloud"></div>
            <div class="snow">
              <div class="flake"></div>
              <div class="flake"></div>
            </div>
          </div>
        )
    }

    else if (props.code === 1000) {
        // Sunny
        if (currentTime > sunrise.getHours() && currentTime < sunset.getHours()) {
            condition = (
                <div className="icon sunny">
                    <div className="sun">
                    <div className="rays"></div>
                    </div>
                </div>
            )
        } else {
            condition = (
                <div className="icon cloudy">
                    <img src={moon} className='moon' alt=""/>
                </div>
            )
        }
    }

    return (
        <div>
            {condition}
        </div>
    )
}

export default Icons;
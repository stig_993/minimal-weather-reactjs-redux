import React from 'react';
import './Search.css';
import {DebounceInput} from 'react-debounce-input';

const SearchForm = (props) => {
    return (
        <div>
            <form>
            <DebounceInput
                minLength={2}
                debounceTimeout={300}
                name='city'
                id='city'
                onChange={props.search} placeholder="City" />
            </form>
        </div>
    )
}

export default SearchForm;
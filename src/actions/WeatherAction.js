import {GET_CURRENT_WEATHER, GET_CITIES, EMPTY_CITIES_LIST, GET_FORECAST} from './types';
import axios from 'axios';

const formatTime = () => {
    let date = new Date();
    let formatted;
    return formatted = date.getHours() + ":" + date.getMinutes();
}

const apiKey = "9009875f9c1b4759993132039183007&q";

export const getCities = (city) => dispatch => {
    if (city !== '') {
        axios({
            method: "GET",
            url: "https://api.apixu.com/v1/search.json?key=" + apiKey + "=" + city
        }).then((res) => {
            dispatch({
                type: GET_CITIES,
                payload: res.data
            })
        })
    } else {
        dispatch({
            type: EMPTY_CITIES_LIST
        })
    }
}

export const getWeather = (city) => dispatch => {
    let url = '';

    if (city === undefined) {
        url = "https://api.apixu.com/v1/current.json?key=" + apiKey + "&q=nis,rs";
    } else {
        url = "https://api.apixu.com/v1/current.json?key=" + apiKey + "&q=" + city ;
    }

    axios({
        method: "GET",
        url: url
    }).then((res) => {
        dispatch({
            type: GET_CURRENT_WEATHER,
            payload: {
                data: res.data, 
                update_time: formatTime(), 
                locationTime: res.data.location.localtime }
        })
    })
}

export const getMyLocWeather = (location) => dispatch => {
    axios({
        method: "GET",
        url: "https://api.apixu.com/v1/current.json?key=" + apiKey + "&q=" + location.lat + ',' + location.lon
    }).then((res) => {
        dispatch({
            type: GET_CURRENT_WEATHER,
            payload: {
                data: res.data, 
                update_time: formatTime(),
                locationTime: res.data.location.localtime
            }
        })
    })

    axios({
        method: "GET",
        url: "https://api.apixu.com/v1/forecast.json?key=" + apiKey + "&q=" + location.lat + ',' + location.lon + "&days=7"
    }).then((res) => {
        dispatch({
            type: GET_FORECAST,
            payload: {
                data: res.data,
                locationTime: res.data.location.localtime
            }
        })
    })
}

export const getForecast = (city) => dispatch => {
    let url;
    if (city === undefined) {
        url = "https://api.apixu.com/v1/forecast.json?key=" + apiKey + "&q=nis,rs&days=7"
    } else {
        url = "https://api.apixu.com/v1/forecast.json?key=" + apiKey + "&q=" + city + "&days=7"
    }
    axios({
        method: "GET",
        url: url
    }).then((res) => {
        dispatch({
            type: GET_FORECAST,
            payload: {
                data: res.data,
                locationTime: res.data.location.localtime
            }
        })
    })
}
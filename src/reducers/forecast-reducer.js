import {GET_FORECAST} from '../actions/types';

const initialState = {
    forecast: [],
    locationTime: 0,
}

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_FORECAST :
            return {
                ...state,
                forecast: action.payload.data,
                locationTime: action.payload.locationTime
            }
        
        default: 
            return state;
    }
}
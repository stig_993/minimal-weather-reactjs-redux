import {GET_CURRENT_WEATHER, GET_CITIES, EMPTY_CITIES_LIST} from '../actions/types';

const initialState = {
    weather_now: {},
    locationTime: 0,
    cities: []
}

export default function(state = initialState, action) {
    switch(action.type) {
        case GET_CURRENT_WEATHER :
            return {
                ...state,
                weather_now: action.payload.data,
                time: action.payload.update_time,
                locationTime: action.payload.locationTime,
                cities: []
            }

        case GET_CITIES :
            return {
                ...state,
                cities: action.payload
            }

        case EMPTY_CITIES_LIST :
            return {
                ...state,
                cities: []
            }
            
        default : 
            return state;
    }
}